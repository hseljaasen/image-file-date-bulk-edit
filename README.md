# image-file-date-bulk-edit
This python scripts aims at setting the creation date of scanned photo to the actual date of the picture.

The basis of this project is to set correct creation time on digitized images. 
For photos the script assume that you save each folder into a digital folder with filenames following step 1 in this convension: 
https://www.scanyourentirelife.com/what-everybody-ought-know-when-naming-your-scanned-photos-part-1/
Image file content will not be altered or considered. The date given will be taken from its mother folder name. 

Start filenames for folder names with YYYY-MM-DD and use XX or xx when in doubt.
