import argparse
import os
import time
import subprocess 
import calendar
import piexif
import piexif.helper
from ctypes import windll, wintypes, byref
import datetime

#Photo:
# Runs over entire directory hunting for folders starting with YYYY-MM-DD format
# For all image files inside such a folder, the creation time and image taken time is 
# edited accordingly
#Movies
#Not yet implemented.

def handleFilesInFolder(root_folder, skipp_files_with_correct_date, photographer):
    filelist = []
    print('\n\n\n---------------------------------- Image Creation Date Update -----------------------------')
    print('\nRoot folder: {root_folder}\n\nHandling subfolders:'.format(root_folder=root_folder))
    image_count = 0
    skipped_count = 0
    allready_fine = 0
    failing_folders = []
    folder_time_shift = datetime.timedelta(minutes=10);
    folder_dates_used = set()
    folder_date_dictionary = {}
    last_root = ''
    for root, dirs, files in os.walk(root_folder):
        for folder in dirs:
            print(folder) 
        for file in files:
            if file.endswith(".jpg") or file.endswith(".tif") or file.endswith(".tiff"):
                full_path = os.path.join(root,file)
                filelist.append(full_path)
                folder_info = getDateTimeFolderPath(root)
                epoch = None
                if folder_info:
                    folder_date = folder_info[0]
                    if root not in folder_date_dictionary:
                        while folder_date in folder_dates_used:
                            folder_date = folder_date + folder_time_shift
                        folder_dates_used.add(folder_date)
                        folder_date_dictionary[root] = folder_date
                    else:
                        folder_date = folder_date_dictionary[root]
                    folder_description = folder_info[1]
                    epoch = getEpocFromDateTime(folder_date)
                if epoch is None:
                    if root not in root_folder:
                        skipped_count += 1
                        if root not in failing_folders:
                            failing_folders.append(root)
                    continue
                if skipp_files_with_correct_date:    
                    originalCreateTime = os.path.getctime(full_path) 
                    if(originalCreateTime == epoch):
                        allready_fine += 1
                        continue
                if file.endswith(".jpg"):
                    setImageExifJpegAttribute(full_path, folder_date, folder_description, photographer)
                elif last_root != root:
                    last_root = root
                    print(root)
                    setImageDateTakenTiffAttribute(root, folder_date, folder_description, photographer)
                setCreationTime(full_path, epoch)
                #setLastModifiedTime(full_path, epoch)
                image_count += 1


 
 
    print('\nDone!\n{count} images updated.\n{skipped_count} images skipped because of failure.\n{allready_fine} images where allready up to date and was skipped.\n'.
        format(count=image_count, skipped_count=skipped_count, allready_fine=allready_fine))    
    if len(failing_folders) > 0:
        print('Skipped folders:')
    for folder in failing_folders:
        print(folder) 
    print('')
    os.system("pause")

def setLastModifiedTime(filepath, epochtime):
    now_epoch = datetime.datetime.now().timestamp()
    os.utime(filepath, (now_epoch, epochtime))

## From https://stackoverflow.com/a/56805533/3407324
def setCreationTime(filepath, epochtime):
    # Convert Unix timestamp to Windows FileTime using some magic numbers
    # See documentation: https://support.microsoft.com/en-us/help/167296
    timestamp = int((epochtime * 10000000) + 116444736000000000)
    ctime = wintypes.FILETIME(timestamp & 0xFFFFFFFF, timestamp >> 32)
    # Call Win32 API to modify the file creation date
    handle = windll.kernel32.CreateFileW(filepath, 256, 0, None, 3, 128, None)
    windll.kernel32.SetFileTime(handle, byref(ctime), None, None) #(handle, ctime, atime, mtime)
    windll.kernel32.CloseHandle(handle)


def setImageDateTakenTiffAttribute(file_or_folder_path, date_time, folder_description, photographer):
    new_date = date_time
    # -P for perserving date modified, -overwrite_original_in_place for perserving file creation date
    # call to wait for the process, Popen to start it in the background
    subprocess.call([
        'exiftool', 
        '-charset', 'FileName=latin',
        '-charset', 'latin1',
        f'-P',  # To avoid updating File Modified time...
        f'-m', 
        f'-overwrite_original_in_place', 
        f'-EXIF:DateTimeOriginal={new_date}', # Modern Exif field, used by i.e. Dropbox for sorting, set both for consistency.
        f'-XMP:DateTimeOriginal={new_date}', # Modern Exif field, used by i.e. Dropbox for sorting, set both for consistency.
        f'-EXIF:CreateDate={new_date}',
        f'-XMP:CreateDate={new_date}', # Old xmp value, which is the only provided metadata format from Nikon Scan
        f'-XMP:Description={folder_description}', # Add description from folder name to metadata
        # Does not support nordic characters: f'-EXIF:ImageDescription={folder_description}',
        f'-XMP:UserComment={folder_description}',
        # Does not support nordic characters: f'-EXIF:UserComment={folder_description}',
        f'-Author={photographer}', 
        f'-XMP:Creator={photographer}',      
        f'-XMP:Rights={photographer}',   
        #f'-IPTC:Caption' f'-Abstract={folder_description}',  # Used by google photo on new uploads
        file_or_folder_path])

def setImageExifJpegAttribute(filename, date_time, image_description, photographer):
    exif_dict = piexif.load(filename)
    exif_dict['Exif'] = { 
        piexif.ExifIFD.DateTimeOriginal: date_time,
        piexif.ExifIFD.UserComment: piexif.helper.UserComment.dump(image_description),
    } 
    exif_dict["0th"][piexif.ImageIFD.ImageDescription]=image_description
    exif_dict["0th"][piexif.ImageIFD.Artist]=photographer
    exif_dict["0th"][piexif.ImageIFD.XPAuthor]=piexif.helper.UserComment.dump(photographer)
    exif_bytes = piexif.dump(exif_dict)
    piexif.insert(exif_bytes, filename)

def getDateTimeFolderPath(folder_path):
    folder_name = os.path.basename(folder_path)
    folder_date_string = folder_name.split()[0]
    folder_info = folder_name.split(' ', 1)
    if len(folder_info) > 1:
        folder_description = folder_info[-1]
    else:
        folder_description =''
                    
    date_info = str.split(folder_date_string,'-')
    if len(date_info) > 2:
        year = date_info[0]
        if 'x' in year.lower():
            return None

        month = date_info[1].lower().replace('xx','01').replace('x','1') #.replace('hh','09').replace('vv','03')
        if not month.isnumeric():
            return None

        date = date_info[2].lower().replace('xx','01').replace('x','1')
        if not date.isnumeric():
            return None
        # Choosing 12:00 AM on the given date to avoid date shifts resulting from daylight savings issues 
        dateString = '{date}:{month}:{year} 12:00'.format(date=date, month=month, year=year)
        return (datetime.datetime.fromtimestamp(time.mktime(time.strptime(dateString, "%d:%m:%Y %H:%M"))), folder_description);
    else:
        return None


def getEpocFromDateTime(datetime):
    if datetime is None:
        return None
    timeshift = getUtcTimeDiff()
    return datetime.timestamp() + timeshift 


def getUtcTimeDiff():
    utcTime = datetime.datetime.utcnow().timestamp()
    localTime = datetime.datetime.now().timestamp()
    timeDiff = utcTime - localTime  
    return timeDiff



if __name__  == '__main__':
    parser = argparse.ArgumentParser(description='Root path')
    parser.add_argument('--src_root_path', help='Root path of the digitalized files', default='ASK')
    parser.add_argument('--src_ignore_files_with_correct_date', help='Ignore files with correct date y/n', default='ASK')
    parser.add_argument('--photographer', help='Name of photographer', default='ASK')

    args = parser.parse_args()
    src_root_path = args.src_root_path
    src_ignore_files_with_correct_date = args.src_ignore_files_with_correct_date
    photographer = args.photographer

    if src_root_path == 'ASK':
        src_root_path = input("Enter the root path of the digitalized files: ")

    if photographer == 'ASK':
        photographer = input("Name of Photographer (leave blank if none): ")

    if src_ignore_files_with_correct_date == 'ASK':
        src_ignore_files_with_correct_date = input("Ignore files with correct date y/n?: ")

    if src_ignore_files_with_correct_date.lower() == 'y':
        ignore_correct = 1;    
    else:
        ignore_correct = 0;
    if not src_root_path:
        print('Invalid path!')
    else:
        handleFilesInFolder(src_root_path, ignore_correct, photographer)
